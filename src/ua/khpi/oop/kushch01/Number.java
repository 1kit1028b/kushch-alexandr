package ua.khpi.oop.kushch01;

public class Number {
    public static final int CONST_A = 65;

    private long number;            // само число

    private byte count_pair;        // счетчик парныъ
    private byte count_no_pair;     // счетчик не парных

    private byte count_one;         // счетчик  единиц
    private String binary;

    Number(){}

    Number(long number){
        this.number = number;
        this.count_pair = 0;
        this.count_no_pair = 0;
        this.count_one = 0;
    }
    public static void main(String [] args) {
        Number list = new Number(0x468b);               // 18059
        Number mobile = new Number(380683191521l);		// Pair: 8,0,6,8,2; No Pair: 3,3,1,9,1,5,1
        Number last_mobile2 = new Number(0b10101);		// 21
        Number last_mobile4 = new Number(02761);		// 1521

        long int_symbol = ((list.number - 1) % 26) + 1;

        char chr_symbol = (char)(CONST_A + int_symbol);

        list.count_number_paired_and_no();
        list.count_binary_ones();

        mobile.count_number_paired_and_no();
        mobile.count_binary_ones();

        last_mobile2.count_number_paired_and_no();
        last_mobile2.count_binary_ones();

        last_mobile4.count_number_paired_and_no();
        last_mobile4.count_binary_ones();

        int symb_count_pair = 0;
        int symb_count_no_pair = 0;
        int symb_count_one = 0;

        char temp = chr_symbol;

        while(temp != 0){
            if(temp % 2 == 0){
                symb_count_pair++;
            }
            else{
                symb_count_no_pair++;
            }
            temp /= 10;
        }

        String bin_symbol = Integer.toBinaryString(chr_symbol);

        for(int i = 0; i < bin_symbol.length(); i++){
            if(bin_symbol.charAt(i) == '1'){
                symb_count_one++;
            }
        }
    }

    public void count_number_paired_and_no(){
        long temp = this.number;

        while(temp != 0){
            if(temp % 2 == 0){
                this.count_pair++;
            }
            else{
                this.count_no_pair++;
            }
            temp /= 10;
        }
    }

    public void count_binary_ones(){
        this.binary = Long.toBinaryString(this.number);

        for(int i = 0; i < this.binary.length(); i++){
            if(this.binary.charAt(i) == '1'){
                this.count_one++;
            }
        }
    }
}
